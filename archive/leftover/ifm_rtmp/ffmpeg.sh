#!/bin/bash

image='leftover_notext480.jpg'
stream='http://deathstar.puscii.nl:8000/papillon.ogg'
text_file='thetext.txt'
font='BPmonoBold.ttf'
rtmp_user=''
rtmp_pass=''
rtmp_url="rtmp://$rtmp_user:$rtmp_pass.@intergalactic.tv/show/leftover"

ffmpeg -loop 1 -framerate 30  -i $image -i $stream -c:v libx264 -pix_fmt yuv420p -vf "drawtext=textfile=$text_file:reload=1:fontfile=$font:y=h-line_h-10:x=w-mod(max(t-4.5\,0)*(w+tw)/10.5\,(w+tw)):fontcolor=00dc90:fontsize=40:shadowx=2:shadowy=2" -c:a aac -ar 44100  -f flv $rtmp_url
