#!/usr/bin/env python3

import pprint

pp = pprint.PrettyPrinter()

import xml.etree.ElementTree as ET
root = ET.parse('stats.txt').getroot()

pp.pprint(root)


for application in root.find('server').findall('application'):
    app = application.find('name').text

    if app == 'ingest':
        for stream in  application.find('live').findall('stream'):
            print(stream.find('name').text)
            print(stream.tag, stream.attrib)

for type_tag in root.findall('bar/type'):
    value = type_tag.get('foobar')
    print(value)
