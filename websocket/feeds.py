#!/usr/bin/env python3
from datetime import datetime
import pprint
from obswebsocket import obsws, requests  # noqa: E402
import asyncio
import websockets
import json 
from sms_config import SmsConfig
from aio_timers import Timer


pp = pprint.PrettyPrinter()


class FeedInput:
    def __init__(self, name):
        self.conf = SmsConfig()
        self.name = name
        self.lastseen = datetime.now()
        self.url = self.conf.rtmp_input_uri + '/' + self.name

    def makeVlcSettings (self):
        mediasettings = {
                'loop': True,
                'network_caching': 400,
                'playback_behavior': 'always_play',
                'playlist': [{  'hidden': False,
                                'selected': False,
                                'value': self.url}],
                'shuffle': False,
                'subtitle': 1,
                'subtitle_enable': False,
                'track': 1
     }

    def isOkObs (self, obsws, feedname):
        # should check whether is still playing in obs
        return True

    def isOkRtmp(self):
        #checks whether its still live on server
        return True

class ObsController:
    """
        chooses which feeds on rtmp will appear as sources in obs
    """
    def __init__(self, obsws):
        self.obsws = obsws
        self.feeds = {
                "FEED1": None,
                "FEED2": None,
                "FEED3": None,
                "FEED4": None,
                "FEED5": None,
                "FEED6": None
        }

    def checkfeeds(self):
        """
            This will check which feeds need replacing with a fresh one, and then do that
        """
        for k, f in self.feeds.items():
            print ("==== ", k, "state")
            if f == None:
                print(k, "empty")
            else:
                if f.isOkObs():
                    print("still fine in obs")
                elif f.isOkRtmp():
                    print("stil fine in rtmp")





if __name__ == '__main__':
    print('name is main')

    conf = SmsConfig()

    host = "localhost"
    port = 4444
    password = "password"

    obsws = obsws(host, port, password)
    obsws.connect()
    
    obscontroller = ObsController(obsws)

    inputs = {
            "test1": FeedInput("test1")
            }

    def onmessage(message):
        print(str(message))

    async def rtmp_events():
        async with websockets.connect(conf.rtmp_ws_uri, ping_interval=None) as websocket:
            await websocket.send(str(json.dumps({"hello": "server"})))
            
            print("waiting for messages on websocket")
            while True:
                if websocket == None:
                    websocket = websockets.connect(conf.rtmp_ws_uri, ping_interval=None)
                else:
                    try:
                        data = await websocket.recv()
                        onmessage(data)
                    except Exception as err:
                        print(err, websocket)

    def feed_checks():
        obscontroller.checkfeeds()
        #check again after 2 sec
        timer = Timer(2, feed_checks, callback_args=())

    feed_checks()
    asyncio.get_event_loop().run_until_complete(rtmp_events())
   
