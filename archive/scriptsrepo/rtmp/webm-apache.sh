#!/bin/bash

AUDIO_OPTIONS="-acodec libvorbis -aq 90 -ac 2"
VIDEO_OPTIONS="-threads 8 -f webm -vcodec libvpx -b 200k"
ffmpeg -y -i $1 $AUDIO_OPTIONS $VIDEO_OPTIONS -f webm "http://flu.v89.eu/video/stream/pinknoise.webm"
