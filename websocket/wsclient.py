#!/usr/bin/env python3

# WS client example

import asyncio
import websockets
import json 
import datetime 

async def hello():
    uri = "ws://localhost:1234/ws"
    async with websockets.connect(uri) as websocket:
        now = datetime.datetime.utcnow().isoformat() + 'Z'
        #await websocket.send(now)
        await websocket.send(str(json.dumps({"test": "lalala"})))
        
        while True:
            data = await websocket.recv()
            print(str(data))

asyncio.get_event_loop().run_until_complete(hello())
