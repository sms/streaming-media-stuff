from setuptools import setup, find_packages

setup(
  name='webhook_relay',
  version='0.1',
  description='relay webhook',
  author='sms',
  maintainer='sms',
  author_email='sms@la.la',
  maintainer_email='sm@la.la',
  install_requires=[
    'aiohttp',
    'websocket'
  ],
  package_dir={
    'webhook_relay': 'webhook_relay',
    'webhook_relay.lib': 'webhook_relay/lib'
  },
  packages=['webhook_relay', 'webhook_relay.lib'],
  entry_points={
    'console_scripts': ['webhook-relay=webhook_relay.main:main']
  }
)
