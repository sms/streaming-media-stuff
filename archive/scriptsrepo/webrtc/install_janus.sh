#!/bin/sh

#  !!! README !!!
#  There is something with janus not accepting confdir or plugindir configuration
# , so put it in the default dir (/usr/local/etc/janus/)  
#

aptitude install libmicrohttpd-dev libjansson-dev libnice-dev \
      libssl-dev libsrtp-dev libsofia-sip-ua-dev libglib2.0-dev \
          libopus-dev libogg-dev libini-config-dev libcollection-dev \
              pkg-config gengetopt libtool automake

#^ that is enough for:
# ./configure --disable-websockets --disable-data-channels --disable-rabbitmq --disable-docs

#docs
#apt-get install doxygen graphviz


#for websocket support libwebsock (git clone git://github.com/payden/libwebsock.git)
apt-get install libevent-dev libevent-pthreads-2.0-5

git clone git://github.com/payden/libwebsock.git
cd libwebsock
git checkout tags/v1.0.4
autoreconf -i
./autogen.sh
./configure && make && sudo make install
ldconfig
cd ../
git clone https://github.com/meetecho/janus-gateway.git
cd janus-gateway
./autogen.sh
./configure --prefix=/opt/janus --disable-docs  --disable-data-channels --disable-rabbitmq
make
make install
