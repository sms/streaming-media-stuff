#!/usr/bin/env bash
#set -x

CLICKCMD="/home/user/src/streaming-media-stuff/panelplugin/switch.sh"
#CLICK="<txtclick>/home/user/src/streaming-media-stuff/panelplugin/switch.sh</txtclick>\n<tool>Click here to change radio status</tool>\n<txtclick>/home/user/src/streaming-media-stuff/panelplugin/switch.sh</txtclick>"
#CLICK="<txtclick>xterm</txtclick>\n<click>xterm</click>"
CLICK="<txtclick>bash $CLICKCMD</txtclick>\n<click>$CLICKCMD</click>"

if [ "$1" = "switchirc" ]
then
	switch=1
fi

if [ "$1" = "switch" ]
then
	switch=1
fi


function isa ()
{
  systemctl --user is-active --quiet liquidsoap.service
}

if isa
then
	img="<img>/home/user/src/streaming-media-stuff/panelplugin/onair-30px.png</img>"
	txt="<txt>ON-low</txt>"
	newstate="off"
else
	txt="<txt>Radio: OFF</txt>"
	newstate="on"
fi

if [ "$switch" = 1 ]
then

  if [ "$newstate" = "on" ]
  then
	    echo "Turning on"
      systemctl --user start liquidsoap.service
  elif [ "$newstate" = "off" ]
  then
	    echo "Turning off"
	    sleep 0.1
      systemctl --user stop liquidsoap.service
  fi
	switch==1
else
	if [ "$1" == 'statusirc' ]
	then
		echo $txt
	else
     	  echo -e "$CLICK\n"
	  echo -e "$img\n"
	  echo $txt
	  echo $newstate
	fi
fi

