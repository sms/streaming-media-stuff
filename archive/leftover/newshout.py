#!/usr/bin/env python3
import getpass
import sys
import telnetlib
import datetime
import sqlite3


conn = sqlite3.connect('leftover.db')
#conn = sqlite3.connect(':memory:')
c = conn.cursor()


now = datetime.datetime.now()
c.execute('''INSERT INTO shouts(created_at,name,message) VALUES(?,?,?)''', (now, sys.argv[1], sys.argv[2] ))

conn.commit()
conn.close()

