
#pixel-aspect-ratio\=\(fraction\)1/1\,\ interlace-mode\=\(string\)progressive\,\ framerate\=\(fraction\)5/1"
#/GstPipeline:pipeline0/GstXvImageSink:xvimagesink0.GstPad:sink: caps = "video/x-raw\,\ format\=\(string\)YUY2\,\ width\=\(int\)1600\,\ height\=\(int\)1200\,\ pixel-aspect-ratio\=\(fraction\)1/1\,\ interlace-mode\=\(string\)progressive\,\ framerate\=\(fraction\)5/1"


/usr/bin/gst-launch-1.0 -vvv \
  rtpbin name=rtpbin \
  v4l2src ! \
  queue ! queue !   videoconvert ! videorate '!' videoscale ! 'video/x-raw, format=I420,width=720,height=576' ! \
  jpegenc ! \
  rtpjpegpay '!'\
  rtpbin.send_rtp_sink_0 rtpbin.send_rtp_src_0 '!'\
  multiudpsink multicast-iface=eth0 clients=224.1.1.2:9996 \
  rtpbin.send_rtcp_src_0 '!' \
  multiudpsink multicast-iface=eth0 clients=224.1.1.2:9997 sync=false async=false \
  udpsrc port=10000 '!' \
  rtpbin.recv_rtcp_sink_0 \
  alsasrc provide-clock=false '!' \
  audioconvert '!' \
  audioresample '!' queue '!' audioconvert '!' \
  rtpL16pay '!' rtpbin.send_rtp_sink_1 \
  rtpbin.send_rtp_src_1 '!' \
  multiudpsink  multicast-iface=eth0 clients=224.1.1.2:9998

