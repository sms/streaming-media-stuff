with import <nixpkgs> {};

stdenv.mkDerivation rec {
  name = "python-environment";

#  buildInputs = [ pkgs.python36 pkgs.python36Packages.flask pkgs.ffmpeg pkgs.python36Packages.gtk ];
  buildInputs = [ 
    #pkgs.python27Packages.pygtk 
    #pkgs.python27Packages.pygobject2
    #pkgs.python27Packages.pygobject3
    #pkgs.python27Packages.django
    #pkgs.python27Packages.django_appconf
    #pkgs.python27Packages.django_extensions
#    python36Packages.django_2_1
#    python36Packages.django-jinja
#    python36Packages.pytaglib
    python36Packages.simplejson
#    python36Packages.pysqlite
    python36Packages.bootstrapped-pip
    #python36Packages.pip
#    taglib_1_9
    sqlite
    madonctl
    toot
#    sopel

];

  shellHook = ''
    #export FLASK_DEBUG=1
    #export FLASK_APP="main.py"
    export SMS="/home/user/src/streaming-media-stuff/"
    export SNOWMIX="/home/user/src/streaming-media-stuff/snowmix"
    export LEFTOVERDB="$SMS/leftover/leftover.db"
    #export API_KEY="some secret key"
  '';
}
