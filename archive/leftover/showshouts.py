#!/usr/bin/python

import sqlite3


conn = sqlite3.connect('/home/user/src/streaming-media-stuff/leftover/leftover.db')
c = conn.cursor()

c.execute('''SELECT * FROM (
             SELECT created_at, name, message FROM shouts ORDER BY created_at DESC LIMIT 6
             )
             ORDER BY created_at ASC''')

shout = ""

for row in c.fetchall():
    shout = shout + ' -- ' + row[1] + ': ' + row[2]
shout = shout + ''
conn.close()

print(shout)

