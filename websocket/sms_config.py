import os

class SmsConfig:
    def __init__(self):
        
        self.input_host = os.environ.get('SMS_INPUT_HOST', 'localhost')

        
        self.obs_host = "localhost"
        self.rtmp_input_uri = "rtmp://" + self.input_host + "/input"
        self.rtmp_ws_uri = "ws://localhost:1234/ws"
        self.rtmp_input_stats_uri = "http://" + self.input_host + "/stats"


