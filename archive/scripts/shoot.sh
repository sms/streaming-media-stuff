#!/usr/bin/env bash

HOST="$1"
USER="root"

mkdir -p ~/sand
cd ~/sand
if ssh $USER@$HOST /home/user/src/streaming-media-stuff/scripts/screenshot.sh
then
  echo "OK"
  scp $USER@$HOST:/home/user/screenshot/latest.png ~/sand/$HOST.png
  eog ~/sand/$HOST.png

else
  echo "failed"
fi
