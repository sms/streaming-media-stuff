#!/usr/bin/env bash

H=$1

cd ~/sand
ssh root@$H /home/user/src/streaming-media-stuff/scripts/screenshot.sh
scp root@$H:/home/user/screenshot/latest.png ~/sand/latest-$H.png
eog ~/sand/latest-$H.png

