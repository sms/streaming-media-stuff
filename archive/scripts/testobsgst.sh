#!/bin/bash


obspipe="audiotestsrc is-live=true ! audio. rtspsrc latency=1 location="rtsp://admin:admin@10.205.12.87/live/h264/ch1" ! decodebin ! videoconvert ! video."


gst-launch-1.0 -v \
 $obspipe \
  xvimagesink name=video \
  fakesink name=audio


#  ! video. \
#  audiotestsrc wave=ticks is-live=true \
#  ! audio/x-raw, channels=2, rate=44100 ! audio. \
#   video. ! xvimagesink \
#    audio. ! fakesink
