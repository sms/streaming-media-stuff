#!/usr/bin/env python3
import asyncio
import signal
from contextlib import suppress
import pprint
import pulsectl_asyncio
import os
import time

pp = pprint.PrettyPrinter(indent=4)
import pulsectl
#print('Event types:', pulsectl.PulseEventTypeEnum)
#print('Event facilities:', pulsectl.PulseEventFacilityEnum)
#print('Event masks:', pulsectl.PulseEventMaskEnum)

active_feeds = ['FEED1', 'FEED2', 'FEED3', 'FEED4', 'FEED5', 'FEED6', 'PLAYER1', 'PLAYER2']

pulse = pulsectl.Pulse()
current_sinks = pulse.sink_input_list()
#print(current_sinks[1].index)
for i in current_sinks:
    print('sinkindex:', i.index, ' sinkname:', i.name)

#    if (i.name == 'FEED1'):
#        print('FEED1 sink event detected, index:', i.index) 
def init_sinks():
    current_sinks = pulse.sink_input_list()
    for i in current_sinks:
        if i.name in active_feeds:
            connect_sink(i.index)

def connect_sink(index):
    current_sinks = pulse.sink_input_list()
    for i in current_sinks:
        if i.index == index:
            if i.name in active_feeds:
                print('feed', i.name, 'with index', i.index, 'detected, connecting port...')
                pacmd_cmd = 'pacmd move-sink-input ' + str(i.index) + ' '  + i.name
                os.system(pacmd_cmd)
            return i.name

async def listen():
    async with pulsectl_asyncio.PulseAsync('event-printer') as pulse:
        async for event in pulse.subscribe_events('sink-input'):
            if event.t == 'new':
                connect_sink(event.index)
            if event.t == 'remove':
                print('removed sink nr:', event.index)
            


            #pp.pprint(event)


async def main():
    # Run listen() coroutine in task to allow cancelling it
    listen_task = loop.create_task(listen())

    # Schedule listen_task to be cancelled after 10 seconds
    # Alternatively, the PulseAudio event subscription can be ended by breaking/returning from the `async for` loop
    #loop.call_later(5, listen_task.cancel)

    # register signal handlers to cancel listener when program is asked to terminate
    for sig in (signal.SIGTERM, signal.SIGHUP, signal.SIGINT):
        loop.add_signal_handler(sig, listen_task.cancel)

    with suppress(asyncio.CancelledError):
        await listen_task


# Run event loop until main_task finishes
time.sleep(15)
init_sinks()
loop = asyncio.get_event_loop()
loop.run_until_complete(main())


