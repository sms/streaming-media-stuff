#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import time

import logging
logging.basicConfig(level=logging.INFO)
import pprint
sys.path.append('../')
from obswebsocket import obsws, requests  # noqa: E402

pp = pprint.PrettyPrinter()

host = "localhost"
port = 4444
password = "password"

ws = obsws(host, port, password)
ws.connect()

def makeVlcSettings (name):
    return {'loop': True,
 'network_caching': 400,
 'playback_behavior': 'always_play',
 'playlist': [{'hidden': False,
               'selected': False,
               'value': 'rtmp://hlsdev.laglab.org/input/preconfigured1' + name}],
 'shuffle': False,
 'subtitle': 1,
 'subtitle_enable': False,
 'track': 1}

# requests are here: https://github.com/Elektordi/obs-websocket-py/blob/master/obswebsocket/requests.py
try:
    play = ws.call(requests.PlayPauseMedia("PLAYER1", True))
    pause = ws.call(requests.PlayPauseMedia("PLAYER1", True))
    restart = ws.call(requests.RestartMedia("PLAYER1"))
    stop = ws.call(requests.StopMedia("PLAYER1"))

    print(" >>> MEDIASOURCES")
    mediasources = ws.call(requests.GetMediaSourcesList())
    for source in mediasources.getMediaSources():
        name = source['sourceName']
        print(name)
        pp.pprint(source)
        settings = ws.call(requests.GetSourceSettings(name, source['sourceKind'])).getSourceSettings()
        pp.pprint(settings)
        if settings == makeVlcSettings(name):
            print("already setup correctly: ", name)
        else:
            ws.call(requests.SetSourceSettings(name, makeVlcSettings(name), 'vlc_source'))

    print(">>>> SOURCES")
    sources = ws.call(requests.GetSourcesList())
    pp.pprint(sources)
    for source in sources.getSources():
        name = source['name']
        print(name)
        pp.pprint(source)


    scenes = ws.call(requests.GetSceneList())

    for s in scenes.getScenes():
        name = s['name']
        print(u"Switching to {}".format(name))
        ws.call(requests.SetCurrentScene(name))
        time.sleep(2)

    print("End of list")

except KeyboardInterrupt:
    pass


ws.disconnect()
