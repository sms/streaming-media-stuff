#!/bin/bash


apt install -y linux-image-rt-amd64

usermod -aG audio user

cat > /etc/security/limits.d/audio.conf << EOF
# Provided by the jackd package.
#
# Changes to this file will be preserved.
#
# If you want to enable/disable realtime permissions, run
#
#    dpkg-reconfigure -p high jackd

@audio   -  rtprio     95
@audio   -  memlock    unlimited
@audio   -  nice      -19
EOF

apt-get -y install sysfsutils
cat > /etc/sysfs.conf/audio.conf << EOF
devices/system/cpu/cpu0/cpufreq/scaling_governor = performance
devices/system/cpu/cpu1/cpufreq/scaling_governor = performance
devices/system/cpu/cpu2/cpufreq/scaling_governor = performance
devices/system/cpu/cpu3/cpufreq/scaling_governor = performance
class/rtc/rtc0/max_user_freq = 2048

EOF

cat > /etc/sysctl.d/audio.conf << EOF
dev.hpet.max-user-freq=2048
vm.swappiness=10
fs.inotify.max_user_watches=524288
EOF

