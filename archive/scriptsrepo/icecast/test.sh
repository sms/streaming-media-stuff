#!/bin/bash
# Deliver mixer1 output to screen.

#SRC='videotestsrc is-live=true'
SRC="souphttpsrc do-timestamp=true is-live=true location=http://172.31.5.10/axis-cgi/mjpg/video.cgi?fps=60 ! jpegdec"

#SRC='v4l2src '
#MIXERFORMAT='video/x-raw-yuv, width=(int)720, height=(int)576, framerate=(fraction)15/1'
MIXERFORMAT='video/x-raw-yuv, width=(int)640, height=(int)480, framerate=(fraction)15/1'

X264SETTINGS='bitrate=3000 tune=zerolatency speed-preset=5'
THEORASETTINGS='bitrate=500 speed-level=2'

SHOUTSETTINGS='ip=localhost mount=feed1.ogv port=3306 password=icewarm streamname=vmixer.ogv description=blabla '

# h264
#gst-launch-0.10 -v          \
#    $SRC                  ! \
#    $MIXERFORMAT          ! \
#    ffmpegcolorspace      ! \
#    queue                 ! \
#    x264enc $X264SETTINGS ! \
#    h264parse             ! \
#    rtph264pay            ! \
#    queue                 ! \
#    udpsink clients=192.168.254.150:4012 sync=false

while [ 1 ]
do

# theora
gst-launch-0.10 -v          \
    $SRC                  ! \
    videorate ! ffmpegcolorspace      ! \
    $MIXERFORMAT	 ! \
    theoraenc $THEORASETTINGS ! queue ! \
   oggmux name=mux           !  queue ! \
    shout2send $SHOUTSETTINGS    \
     jackaudiosrc client-name=icecast ! queue !\
    audioconvert ! \
    vorbisenc ! queue ! mux. \
 
sleep 4

#     $MIXERFORMAT          ! \

#    videobox top=0 left=0 right=720 bottom=576 ! \

done

#gst-launch-0.10 --eos-on-shutdown v4l2src ! videoscale ! video/x-raw-yuv,width=320,height=240,framerate=30000/1001,interlaced=true ! queue max-size-bytes=100000000 max-size-time=0 ! gamma gamma=1.2 ! queue max-size-bytes=100000000 max-size-time=0 ! videobalance saturation=1.9 brightness=0.00 contrast=1.4 hue=0.06 ! ffmpegcolorspace ! queue max-size-bytes=100000000 max-size-time=0 ! theoraenc bitrate=400 ! queue max-size-bytes=100000000 max-size-time=0 ! oggmux name=mux alsasrc device=hw:1,0 latency-time=100 ! audioconvert ! vorbisenc ! queue max-size-bytes=100000000 max-size-time=0 ! mux. mux. ! queue max-size-bytes=100000000 max-size-time=0 ! shout2send ip=icecast-server.com password=hackme mount=/mountpoint.ogv


#gst-launch-0.10 v4lsrc device=/dev/video0 ! ffmpegcolorspace ! videoscale !
#video/x-raw-yuv,width=320,height=240 ! theoraenc bitrate=80 ! oggmux
#name=mux !  shout2send ip=hackitectura.net port=8000 password=XXXXX
#streamname=http://son0p.tv description= <http://son0p.tvdescription=>"alej00d
#live transmission" mount=son0p.ogg  alsasrc ! audioconvert ! audioresample !
#audio/x-raw-float,width=32,rate=22050,channels=2 ! vorbisenc ! queue ! mux.
