
JPEGCAPS='caps = "application/x-rtp\,\ media\=\(string\)video\,\ clock-rate\=\(int\)90000\,\ encoding-name\=\(string\)JPEG\,\ a-framerate\=\(string\)30.000000\,\ a-framesize\=\(string\)720-576\,\ payload\=\(int\)26\,\ ssrc\=\(uint\)2847158819\,\ timestamp-offset\=\(uint\)3349242841\,\ seqnum-offset\=\(uint\)22236"'
AUDIOCAPS=

gst-launch-1.0 -v rtpbin name=rtpbin \
  udpsrc address=224.1.1.2 port=9996 \
  caps="application/x-rtp\,\ media\=\(string\)video\,\ clock-rate\=\(int\)90000\,\ encoding-name\=\(string\)JPEG\,\ a-framerate\=\(string\)30.000000\,\ a-framesize\=\(string\)720-576\,\ payload\=\(int\)26\,\ ssrc\=\(uint\)2847158819\,\ timestamp-offset\=\(uint\)3349242841\,\ seqnum-offset\=\(uint\)22236" \
  ! rtpbin.recv_rtp_sink_0 \
  rtpbin. ! rtpjpegdepay ! jpegdec ! videorate ! videoconvert ! xvimagesink \
  udpsrc address=224.1.1.2 port=9998 \
  caps="application/x-rtp\,\ media\=\(string\)audio\,\ clock-rate\=\(int\)44100\,\ encoding-name\=\(string\)L16\,\ encoding-params\=\(string\)2\,\ channels\=\(int\)2\,\ payload\=\(int\)96\,\ ssrc\=\(uint\)1986853699\,\ timestamp-offset\=\(uint\)3819158133\,\ seqnum-offset\=\(uint\)16237" \
  ! rtpbin.recv_rtp_sink_1 \
  rtpbin. ! rtpL16depay ! audioconvert ! audioresample ! queue !  alsasink
