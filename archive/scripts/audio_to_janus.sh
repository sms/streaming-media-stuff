#!/usr/bin/env bash


gst-launch-1.0 -v -e jacksrc \
!  audioconvert \
! audioresample \
! audio/x-raw,channels=1,rate=16000 \
! opusenc bitrate=20000 \
! rtpopuspay \
! udpsink host=janus port=5002

