#!/usr/bin/env sh
set -eux
#. /etc/default/sms
#ffmpeg -re -f lavfi -i testsrc=s=hd720,format=yuv420p -f lavfi -i anullsrc -c:v libx264 -g 50 -c:a aac -f flv rtmp://a.rtmp.youtube.com/live2/<stream-key> I'm not much of a network protocol user, but I wonder if a build using --enable-librtmp 

ffmpeg -re \
    -f lavfi -i testsrc=s=hd720,format=yuv420p \
      -f lavfi -i "sine=frequency=1000" \
        -c:v libx264 -g 50 \
          -c:a aac -f flv \
            rtmp://hlsorigindev.laglab.org:9935/stream/leftover

