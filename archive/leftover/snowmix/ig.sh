#!/bin/bash


ffmpeg -f jack -ac 2 -i hw:0,0 -f x11grab -framerate 30 -video_size 640x480 \
	-i :0.0+0,0 -c:v libx264 -preset veryfast -maxrate 198k -bufsize 3968k \
	-vf "format=yuv420p" -g 60 -c:a aac -b:a 128k -ar 44100 \
	-f flv  rtmp://Zombie:Apocalyp3N0w.@intergalactic.tv/show/leftover
