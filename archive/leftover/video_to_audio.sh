#!/usr/bin/env bash 

set -eux

tmp_url="rtmp://dock.lag/stream/leftover"
liquidsoap_input='icecast://source:hackme@dock.lag:8051/pap-c'

while [ true ]
do

ffmpeg -i $rtmp_url \
  -vn \
  -c:a vorbis \  
  -f ogg $liquidsoap_input 

sleep 5

done
