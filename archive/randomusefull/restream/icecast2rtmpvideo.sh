#!/usr/bin/env bash

SRC=http://deathstar.puscii.nl:8000/papillon.mp3



ffmpeg -loop 1  \
-i background_image_at_resolution.jpg \
-i $SRC \
-c:v libx264 \
-vf format=yuv420p -\
vf "drawtext=textfile=/home/dreamer/thetext:reload=1:fontfile=/home/dreamer/.fonts/BPmonoBold.ttf:y=h-line_h-10:x=w-mod(max(t-4.5\,0)*(w+tw)/10.5\,(w+tw)):fontcolor=00dc90:fontsize=40:shadowx=2:shadowy=2" \
-c:a aac \
-ar 44100  \
-f flv rtmp://user:pass@intergalactic.tv/show/leftover
