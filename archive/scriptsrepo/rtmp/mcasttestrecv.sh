#!/bin/bash

#caps = application/x-rtp, media=(string)audio, clock-rate=(int)4800, encoding-name=(string)L16, encoding-params=(string)2, channels=(int)2, ssrc=(uint)3459970867, payload=(int)96, clock-base=(int)2519782556, seqnum-base=(uint)15884 

#caps = application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)JPEG, ssrc=(uint)3650380105, payload=(int)96, clock-base=(uint)891404724, seqnum-base=(uint)27573



gst-launch-0.10 -v gstrtpbin name=rtpbin\
	udpsrc caps="application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)JPEG, ssrc=(guint)469657143, payload=(int)96, clock-base=(guint)2841649723, seqnum-base=(guint)39869" port=9996 !\
	rtpbin.recv_rtp_sink_0 \
	rtpbin. ! rtpjpegdepay !\
	jpegdec ! xvimagesink\
	udpsrc port=9997 ! rtpbin.recv_rtcp_sink_0\
	rtpbin.send_rtcp_src_0 ! multiudpsink clients="127.0.0.1:10000" sync=false async=false\
	udpsrc caps="application/x-rtp, media=(string)audio, clock-rate=(int)44100, encoding-name=(string)L16, encoding-params=(string)2, channels=(int)2, ssrc=(guint)469657143, payload=(int)96, clock-base=(guint)2841649723, seqnum-base=(guint)39869" port=9998 !\
	rtpbin.recv_rtp_sink_1 \
	rtpbin. ! rtpL16depay !\
	autoaudiosink\
	udpsrc port=9999 ! rtpbin.recv_rtcp_sink_1\
	rtpbin.send_rtcp_src_1 ! multiudpsink clients="127.0.0.1:10001" sync=false async=false
