#!/bin/bash

GST_DEBUG=2 gst-launch-1.0 udpsrc uri=udp://239.1.2.3:5004 auto-multicast=true caps='application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)MP2T-ES, payload=(int)33, ssrc=(uint)3027185587, clock-base=(uint)535784712, seqnum-base=(uint)8467' ! queue ! rtpbin \
	! queue! decodebin name=decode \
	! queue ! vp8enc cpu-used=0 target-bitrate=5000 lag-in-frames=25 ! queue ! webmmux streamable=true name=mux ! queue ! \
	shout2send ip=flu.v89.eu port=8000 mount=pinknoise2.webm  decode. ! queue ! audioconvert ! queue ! vorbisenc ! mux.

#GST_DEBUG=2 gst-launch-1.0 udpsrc uri=udp://239.1.2.3:5004 auto-multicast=true caps='application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)MP2T-ES, payload=(int)33, ssrc=(uint)3027185587, clock-base=(uint)535784712, seqnum-base=(uint)8467' ! queue ! rtpbin  \
#	! queue! decodebin name=decode \
#	! queue ! xvimagesink decode. ! queue ! audioconvert ! autoaudiosink



#ffmpeg -re -f rawvideo -s 720x576 -r 30000/1001 -pixel_format yuv420p -i /tmp/video.yuv \
#	-f jack -ac 2 -i ffmpeg\
#	-vcodec libx264 -qscale 2 -r 50 -profile:v baseline -preset ultrafast -ar 44100 -ab 2560 -s 720x576 -vb 100k -f flv 'rtmp://flu.v89.eu/live/stream'


#audio only (works fine)
#ffmpeg \
#	-f jack -ac 2 -i ffmpeg\
#	-ar 44100 -ab 256k -s 720x576  -f flv 'rtmp://flu.v89.eu/live/stream'
