#!/bin/bash
gst-launch-0.10 -v gstrtpbin name=rtpbin\
	videotestsrc ! jpegenc idct-method=2 quality=100 !\
	'image/jpeg, width=(int)720, height=(int)576, framerate=(fraction)25/1' !\
	rtpjpegpay ssrc=9996 timestamp-offset=0 seqnum-offset=0 ! rtpbin.send_rtp_sink_0\
	rtpbin.send_rtp_src_0 ! multiudpsink clients="127.0.0.1:9996"\
	rtpbin.send_rtcp_src_0 ! multiudpsink clients="127.0.0.1:9997" sync=false async=false\
	udpsrc port=10000 ! rtpbin.recv_rtcp_sink_0\
	audiotestsrc ! audioconvert !\
	'audio/x-raw-int, rate=(int)44100, channels=(int)2' !\
	rtpL16pay ssrc=9998 timestamp-offset=0 seqnum-offset=0 ! rtpbin.send_rtp_sink_1\
	rtpbin.send_rtp_src_1 ! multiudpsink clients="127.0.0.1:9998"\
	rtpbin.send_rtcp_src_1 ! multiudpsink clients="127.0.0.1:9999" sync=false async=false\
	udpsrc port=10001 ! rtpbin.recv_rtcp_sink_1
