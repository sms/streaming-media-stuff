#!/bin/bash

export DISPLAY=:0
filename=/home/user/screenshot/`date --iso-8601=seconds`.png

mkdir -p /home/user/screenshot
chown -R user:user /home/user/screenshot

if su user -c "import -window root $filename"
then
  echo "screenshot ok"
  rm /home/user/screenshot/latest.png
  ln -s $filename /home/user/screenshot/latest.png
  exit 0
else
  echo "failed"
  exit 10
fi



