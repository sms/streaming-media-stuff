#!/bin/bash

# jack.client_name = "TALKBACK-01"

pa_source () {
    if pacmd list-sources | grep -q $1
    then
	echo "* source $1 already exists"
    else
	echo "* creating source $1"
	/usr/bin/pacmd load-module module-jack-source channels=2 source_name=$1 client_name=$1 connect=false
    fi
}

pa_sink () {
    if pacmd list-sinks | grep -q $1
    then
	echo "* sink $1 already exists"
    else
	echo "* creating sink $1"
	/usr/bin/pacmd load-module module-jack-sink channels=2 sink_name=$1 client_name=$1 connect=false
    fi
}

while ! jack_lsp
do
  echo "jack is not running"
  sleep 1
done

while ! pacmd list-sinks
do
  echo "pulse not running"
  sleep 1
done

pa_source "TALKBACK"
pa_source "BROADCAST"

pa_sink "PLAYER1"
pa_sink "PLAYER2"

pa_sink "FEED1"
pa_sink "FEED2"
pa_sink "FEED3"
pa_sink "FEED4"
pa_sink "FEED5"
pa_sink "FEED6"


# setup a default sink so that other audio does not interrupt stream
pacmd load-module module-null-sink sink_name=null
pacmd set-default-sink null
